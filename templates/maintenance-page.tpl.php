<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie6"><![endif]-->
<!--[if IE 7 ]><html class="ie7"><![endif]-->
<!--[if IE 8 ]><html class="ie8"><![endif]-->
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html><!--<![endif]-->
<head>
<title><?php print $title; ?> - ANU</title>
<?php print $head; ?>
<?php print $styles; ?>
    <meta name="generator" content="ANU.template GW2-4 | ANU.appid GW-Drupal-7 | ANU.GMS 4.23_20160606 | ANU.inc_from style.anu.edu.au" />
    <meta name="dcterms.publisher" content="The Australian National University" />
    <meta name="dcterms.publisher" content="webmaster@anu.edu.au" />
    <meta name="dcterms.rights" content="http://www.anu.edu.au/copyright/" />
    <meta property="og:image" content="http://<?php print $acton_profile_default_endpoint_base; ?>/4/images/logos/anu_logo_fb_350.png" />
    <link href="//<?php print $acton_profile_default_endpoint_base; ?>/4/images/logos/anu.ico" rel="shortcut icon" type="image/x-icon" />
    <link href="//<?php print $acton_profile_default_endpoint_base; ?>/images/icons/web/anu-app-57.png" rel="apple-touch-icon" sizes="57x57" />
    <link href="//<?php print $acton_profile_default_endpoint_base; ?>/images/icons/web/anu-app-76.png" rel="apple-touch-icon" sizes="76x76" />
    <link href="//<?php print $acton_profile_default_endpoint_base; ?>/images/icons/web/anu-app-120.png" rel="apple-touch-icon" sizes="120x120" />
    <link href="//<?php print $acton_profile_default_endpoint_base; ?>/images/icons/web/anu-app-152.png" rel="apple-touch-icon" sizes="152x152" />
    <link href="//<?php print $acton_profile_default_endpoint_base; ?>/images/icons/web/anu-app-180.png" rel="apple-touch-icon" sizes="180x180" />
    <!--[if !IE]>-->
    <link href="//<?php print $acton_profile_default_endpoint_base; ?>/4/min/gw.min.css?1" rel="stylesheet" type="text/css" media="screen" />
    <!--<![endif]-->
    <!--[if lt IE 8]><link href="//<?php print $acton_profile_default_endpoint_base; ?>/4/min/gw.ie7.min.css?1" rel="stylesheet" type="text/css" media="screen"/><![endif]-->
    <!--[if gt IE 7]><link href="//<?php print $acton_profile_default_endpoint_base; ?>/4/min/gw.ie8.min.css?1" rel="stylesheet" type="text/css" media="screen"/><![endif]-->
    <link href="//<?php print $acton_profile_default_endpoint_base; ?>/4/style/anu-print.css?1" rel="stylesheet" type="text/css" media="print" />
    <!-- jq -->
    <script src="//<?php print $acton_profile_default_endpoint_base; ?>/4/scripts/jquery-1.11.3.min.js?1" type="text/javascript"></script>
    <script type="text/javascript">
        var $anujq = jQuery.noConflict();
    </script>
    <script src="//<?php print $acton_profile_default_endpoint_base; ?>/4/scripts/jquery.hoverIntent.js?1" type="text/javascript"></script>
    <script src="//<?php print $acton_profile_default_endpoint_base; ?>/4/min/anu-common.min.js?1" type="text/javascript"></script>
    <script src="//<?php print $acton_profile_default_endpoint_base; ?>/4/min/anu-mega-menu.min.js?1" type="text/javascript"></script>
    <!-- ejq -->
</head>

<body>

    <!-- noindex -->
    <div id="skipnavholder"><a id="skipnav" href="#content">Skip navigation</a></div>
    <div id="print-hdr">
        <div class="left"><img src="//<?php print $acton_profile_default_endpoint_base; ?>/4/images/logos/anu_logo_print.png" alt="The Australian National University" height="40" width="115" /></div>
        <div class="right"></div>
        <div class="blockline"></div>
    </div>
    <div id="bnr-wrap" class="bnr-gwy-high noborder" role="banner">
        <div id="bnr-gwy" class="bnr-gwy-high">
            <div id="bnr-left">

                <a href="http://www.anu.edu.au/" class="anu-logo-png"><img class="text-white" src="//<?php print $acton_profile_default_endpoint_base; ?>/4/images/logos/2x_anu_logo_small.png" onmouseover="this.src='//<?php print $acton_profile_default_endpoint_base; ?>/4/images/logos/2x_anu_logo_small_over.png';" onfocus="this.src='//<?php print $acton_profile_default_endpoint_base; ?>/4/images/logos/2x_anu_logo_small_over.png';" onmouseout="this.src='//<?php print $acton_profile_default_endpoint_base; ?>/4/images/logos/2x_anu_logo_small.png'" onblur="this.src='//<?php print $acton_profile_default_endpoint_base; ?>/4/images/logos/2x_anu_logo_small.png'" alt="The Australian National University" /></a>
            </div>
            <div id="bnr-low" class="clear">
                <!--GW_UTILITIES-->

            </div>
        </div>
    </div>
    <!--GW_NAV_WRAP-->

    <!-- endnoindex -->

    <div id="body-wrap" role="main">
        <div id="body">

            <!-- endnoindex -->
            <div id="body-wrap">
                <div id="body">
                    <!-- noindex -->
                    <div class="full" style="min-height: 200px;">
                        <h1 class="title">Error</h1>
                        <p>The website encountered an unexpected error. Please try again later.</p>
                    </div>
                    <!-- endnoindex -->
                </div>
            </div>

        </div>
    </div>

    <!--GW_SUBFOOTER-->

    <!-- noindex -->
    <div id="footer-wrap" role="contentinfo" class="gw2-footer">
        <div id="anu-footer">
            <div id="anu-detail">
                <ul>
                    <li><a href="http://www.anu.edu.au/contact">Contact ANU</a></li>
                    <li><a href="http://www.anu.edu.au/copyright">Copyright</a></li>
                    <li><a href="http://www.anu.edu.au/disclaimer">Disclaimer</a></li>
                    <li><a href="http://www.anu.edu.au/privacy">Privacy</a></li>
                    <li><a href="http://www.anu.edu.au/freedom-of-information">Freedom of Information</a></li>
                </ul>
            </div>
            <div id="anu-address">
                <p>+61 2 6125 5111
                    <br/> The Australian National University, Canberra
                    <br/> CRICOS Provider : 00120C
                    <br/>
                    <span class="NotAPhoneNumber">ABN : 52 234 063 906</span></p>
            </div>

            <div id="anu-groups">
                <div class="anu-ftr-go8 hpad vpad">
                    <a href="http://www.anu.edu.au/about/partnerships/group-of-eight"><img class="text-white" src="//<?php print $acton_profile_default_endpoint_base; ?>/4/images/logos/2x_GroupOf8.png" alt="Group of Eight Member" /></a>
                </div>
                <div class="anu-ftr-iaru hpad vpad">
                    <a href="http://www.anu.edu.au/about/partnerships/international-alliance-of-research-universities"><img class="text-white" src="//<?php print $acton_profile_default_endpoint_base; ?>/4/images/logos/2x_iaru.png" alt="IARU" /></a>
                </div>
                <div class="anu-ftr-apru hpad vpad">
                    <a href="http://www.anu.edu.au/about/partnerships/association-of-pacific-rim-universities"><img class="text-white" src="//<?php print $acton_profile_default_endpoint_base; ?>/4/images/logos/2x_apru.png" alt="APRU" /></a>
                </div>
                <div class="anu-ftr-edx hpad vpad">
                    <a href="http://www.anu.edu.au/about/partnerships/edx"><img class="text-white" src="//<?php print $acton_profile_default_endpoint_base; ?>/4/images/logos/2x_edx.png" alt="edX" /></a>
                </div>

            </div>
            <div class="left ie7only full msg-warn" id="ie7-warn">You appear to be using Internet Explorer 7, or have compatibility view turned on. Your browser is not supported by ANU web styles.
                <br>&raquo; <a href="http://webpublishing.anu.edu.au/steps/anu-web-environment/no_more_ie_7.php">Learn how to fix this</a>

                <br><span class="small" id="ignore-ie7-cookie">&raquo; <a href="http://webpublishing.anu.edu.au/steps/anu-web-environment/no_more_ie_7.php?ignore=1" >Ignore this warning in future</a></span>
            </div>
        </div>
    </div>
    <!-- endnoindex -->
</body>

</html>